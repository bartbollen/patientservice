package nl.pharmapartners.services.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.pharmapartners.services.entities.Patient;

@Repository
public interface IPatientRepository extends JpaRepository<Patient, Long>
{
	Patient getByBsn(BigInteger bsn);
}