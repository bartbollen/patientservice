package nl.pharmapartners.services;

import javax.servlet.Servlet;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
@ComponentScan
public class PatientWebServiceConfig extends WsConfigurerAdapter 
{
	@Bean
	public ServletRegistrationBean<Servlet> messageDispatcherServlet(ApplicationContext applicationContext) 
	{
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);

		return new ServletRegistrationBean<>(servlet, "/patient/*");
	}

	@Bean(name = "patient")
	public Wsdl11Definition defaultWsdl11Definition() 
	{
	    SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
	    wsdl11Definition.setWsdl(new ClassPathResource("PatientService_v1.wsdl"));
		
	    return wsdl11Definition;
	}
	
	@Bean
	public XsdSchema patientSchema() 
	{
	    return new SimpleXsdSchema(new ClassPathResource("PatientServiceMessages.xsd"));
	}
}
