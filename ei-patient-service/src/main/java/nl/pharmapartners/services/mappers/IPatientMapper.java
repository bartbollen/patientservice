package nl.pharmapartners.services.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import nl.pharmapartners.services.patientservice.v1.Patient;

@Mapper
public interface IPatientMapper 
{
	IPatientMapper INSTANCE = Mappers.getMapper(IPatientMapper.class); 
	
	Patient patientEntityToPatient(nl.pharmapartners.services.entities.Patient patient);
}

