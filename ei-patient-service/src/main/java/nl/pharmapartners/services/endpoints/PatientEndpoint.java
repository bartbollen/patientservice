package nl.pharmapartners.services.endpoints;

import java.math.BigInteger;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import nl.pharmapartners.services.businessobjects.PatientBO;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsRequest;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ObjectFactory;
import nl.pharmapartners.services.patientservice.v1.Patient;
import nl.pharmapartners.services.patientservice.v1.ResultCodeType;

@Endpoint
public class PatientEndpoint 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PatientEndpoint.class);
	private static final String NAME_SPACE = "http://www.pharmapartners.nl/services/patientservice/v1";
	
	@Autowired
	private PatientBO patientBO;
	
	private JAXBElement<GetPatientDetailsResponse> CreateResponse(Patient patient, ResultCodeType resultCodeType)
	{
		ObjectFactory factory = new ObjectFactory();
		
		GetPatientDetailsResponse getPatientDetailsResponse = factory.createGetPatientDetailsResponse();
		getPatientDetailsResponse.setPatient(patient);
		getPatientDetailsResponse.setResultCode(resultCodeType);
		
		JAXBElement<GetPatientDetailsResponse> result = new JAXBElement<GetPatientDetailsResponse>(new QName(NAME_SPACE, "GetPatientDetailsResponse"), GetPatientDetailsResponse.class, getPatientDetailsResponse);
		
		LOGGER.info("Response endpoint getPatientDetails, result: {}", resultCodeType);
		
		return result;
	}

	@PayloadRoot(namespace = NAME_SPACE, localPart = "GetPatientDetailsRequest")
	@ResponsePayload
	public JAXBElement<GetPatientDetailsResponse> getPatientDetails(@RequestPayload JAXBElement<GetPatientDetailsRequest> getPatientDetailsRequest) 
	{
	    LOGGER.info("Incoming request endpoint getPatientDetails");
	
		BigInteger bsn = getPatientDetailsRequest.getValue().getBSN();
		if (bsn == null)
		{
			LOGGER.error("Received BSN is empty");
			
			return CreateResponse(null, ResultCodeType.BAD_REQUEST);
		}
		
		LOGGER.info("Received BSN: {}", getPatientDetailsRequest.getValue().getBSN());
		
		Patient patient = patientBO.getByBsn(getPatientDetailsRequest.getValue().getBSN());
		
		if (patient == null)
			return CreateResponse(null, ResultCodeType.ERROR);	
	    
		return CreateResponse(patient, ResultCodeType.OK);
	}
}
