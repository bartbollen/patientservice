package nl.pharmapartners.services.businessobjects;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nl.pharmapartners.services.mappers.IPatientMapper;
import nl.pharmapartners.services.patientservice.v1.Patient;
import nl.pharmapartners.services.repositories.IPatientRepository;

@Component
public class PatientBO 
{
	@Autowired
    private IPatientRepository patientRepository;
	
	public Patient getByBsn(BigInteger bsn)
	{
		nl.pharmapartners.services.entities.Patient patient = patientRepository.getByBsn(bsn);
		
		return IPatientMapper.INSTANCE.patientEntityToPatient(patient);
	}
}

