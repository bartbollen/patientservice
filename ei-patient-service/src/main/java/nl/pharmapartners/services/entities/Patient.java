package nl.pharmapartners.services.entities;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import nl.pharmapartners.services.patientservice.v1.GenderType;

@Entity
public class Patient
{
	@Id
    @GeneratedValue
    private Long id;
	
	@Column(unique=true)
	private BigInteger bsn;
    
	@Column
	private String firstName;
    
	@Column
	private String name;
	
	@Column
	private String initials;
	
	@Column
	private Date birthdate;
	
	@Enumerated(EnumType.STRING)
	@Column
	private GenderType gender;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigInteger getBSN() {
		return bsn;
	}

	public void setBSN(BigInteger bsn) {
		this.bsn = bsn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}
}

