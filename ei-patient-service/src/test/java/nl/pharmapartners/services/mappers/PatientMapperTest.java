package nl.pharmapartners.services.mappers;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import nl.pharmapartners.services.patientservice.v1.GenderType;

@RunWith(SpringRunner.class)
public class PatientMapperTest 
{
	@Test
	public void patientEntityToPatient_ReturnPatient()
	{
		nl.pharmapartners.services.entities.Patient patientEntity = new nl.pharmapartners.services.entities.Patient();
		patientEntity.setBSN(new BigInteger("123456789"));
		patientEntity.setFirstName("Bart");
		patientEntity.setName("Bollen");
		patientEntity.setBirthdate(new GregorianCalendar(1988, Calendar.OCTOBER, 2).getTime());
		patientEntity.setGender(GenderType.M);
		
		nl.pharmapartners.services.patientservice.v1.Patient patient = IPatientMapper.INSTANCE.patientEntityToPatient(patientEntity);
		
		assertEquals(patientEntity.getBSN(), patient.getBSN());
		assertEquals(patientEntity.getFirstName(), patient.getFirstName());
		assertEquals(patientEntity.getName(), patient.getName());
		assertEquals(patientEntity.getBirthdate(), patient.getBirthdate().toGregorianCalendar().getTime());
		assertEquals(patientEntity.getGender(), patient.getGender());
	}
}
