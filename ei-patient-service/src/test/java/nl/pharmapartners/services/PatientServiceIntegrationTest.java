package nl.pharmapartners.services;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.*;

import java.io.IOException;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PatientWebServiceConfig.class)
public class PatientServiceIntegrationTest 
{
	@Autowired
    private ApplicationContext applicationContext;
	
	private MockWebServiceClient mockWebServiceClient;
	
	private Resource xsdSchema = new ClassPathResource("PatientServiceMessages.xsd");
	
	@Before
	public void setUp()
	{
		mockWebServiceClient = MockWebServiceClient.createClient(applicationContext);
    }
	
	@Test
	public void getByBsn_ReturnPatient() throws IOException
	{       
        Source requestPayload = new StringSource(
				"<v1:GetPatientDetailsRequest xmlns:v1=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
				"   <v1:BSN>123456789</v1:BSN>" + 
				"</v1:GetPatientDetailsRequest>");

        Source responsePayload = new StringSource(
        		"<ns2:GetPatientDetailsResponse xmlns:ns2=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
        		"   <ns2:Patient>" + 
        		"      <ns2:BSN>123456789</ns2:BSN>" + 
        		"      <ns2:FirstName>Bart</ns2:FirstName>" + 
        		"      <ns2:Name>Bollen</ns2:Name>" + 
        		"      <ns2:Initials>B</ns2:Initials>" + 
        		"      <ns2:Birthdate>1988-10-02+01:00</ns2:Birthdate>" + 
        		"      <ns2:Gender>M</ns2:Gender>" + 
        		"   </ns2:Patient>" + 
        		"   <ns2:ResultCode>OK</ns2:ResultCode>" +
        		"</ns2:GetPatientDetailsResponse>");

        mockWebServiceClient
                .sendRequest(withPayload(requestPayload))
                .andExpect(noFault())
                .andExpect(payload(responsePayload))
                .andExpect(validPayload(xsdSchema));
	}
	
	@Test
	public void getByBsn_ReturnBadRequest() throws IOException
	{       
        Source requestPayload = new StringSource(
				"<v1:GetPatientDetailsRequest xmlns:v1=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
				"   <v1:BSN></v1:BSN>" + 
				"</v1:GetPatientDetailsRequest>");

        Source responsePayload = new StringSource(
        		"<ns2:GetPatientDetailsResponse xmlns:ns2=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
        		"   <ns2:ResultCode>BAD_REQUEST</ns2:ResultCode>" + 
        		"</ns2:GetPatientDetailsResponse>");

        mockWebServiceClient
		        .sendRequest(withPayload(requestPayload))
		        .andExpect(noFault())
		        .andExpect(payload(responsePayload))
		        .andExpect(validPayload(xsdSchema));
	}
	
	@Test
	public void getByBsn_ReturnError() throws IOException
	{       
        Source requestPayload = new StringSource(
				"<v1:GetPatientDetailsRequest xmlns:v1=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
				"   <v1:BSN>1</v1:BSN>" + 
				"</v1:GetPatientDetailsRequest>");

        Source responsePayload = new StringSource(
        		"<ns2:GetPatientDetailsResponse xmlns:ns2=\"http://www.pharmapartners.nl/services/patientservice/v1\">" + 
        		"   <ns2:ResultCode>ERROR</ns2:ResultCode>" + 
        		"</ns2:GetPatientDetailsResponse>");

        mockWebServiceClient
		        .sendRequest(withPayload(requestPayload))
		        .andExpect(noFault())
		        .andExpect(payload(responsePayload))
		        .andExpect(validPayload(xsdSchema));
	}
}
