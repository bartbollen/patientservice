package nl.pharmapartners.services.businessobjects;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import nl.pharmapartners.services.patientservice.v1.GenderType;
import nl.pharmapartners.services.patientservice.v1.Patient;
import nl.pharmapartners.services.repositories.IPatientRepository;

@RunWith(SpringRunner.class)
public class PatientBOTest 
{
	@TestConfiguration
    static class PatientBOTestContextConfiguration 
    {
        @Bean
        public PatientBO patientBO()
        {
        	  return new PatientBO();
        }
    }
		
	@Autowired
	private PatientBO patientBO;
		
	@MockBean
	private IPatientRepository patientRepository;
	
	@Before
	public void setUp() 
	{
		nl.pharmapartners.services.entities.Patient patientEntity = new nl.pharmapartners.services.entities.Patient();
		patientEntity.setBSN(new BigInteger("123456789"));
		patientEntity.setFirstName("Bart");
		patientEntity.setName("Bollen");
		patientEntity.setBirthdate(new GregorianCalendar(1988, Calendar.OCTOBER, 2).getTime());
		patientEntity.setGender(GenderType.M);
		
        when(patientRepository.getByBsn(patientEntity.getBSN())).thenReturn(patientEntity);
    }
	
	@Test
	public void getByBsn_ReturnPatient() throws DatatypeConfigurationException
	{
		Patient patient = new Patient();
		
		patient.setBSN(new BigInteger("123456789"));
		patient.setFirstName("Bart");
		patient.setName("Bollen");
		patient.setBirthdate(DatatypeFactory.newInstance().newXMLGregorianCalendar((new GregorianCalendar(1988, Calendar.OCTOBER, 2))));
		patient.setGender(GenderType.M);
		
		Patient returnPatient = patientBO.getByBsn(patient.getBSN());
				
		assertEquals(returnPatient.getBSN(), patient.getBSN());
		assertEquals(returnPatient.getFirstName(), patient.getFirstName());
		assertEquals(returnPatient.getName(), patient.getName());
		assertEquals(returnPatient.getBirthdate().toGregorianCalendar().getTime(), patient.getBirthdate().toGregorianCalendar().getTime());
		assertEquals(returnPatient.getGender(), patient.getGender());
	}
	
	@Test
	public void getByBsn_ReturnNull() throws DatatypeConfigurationException
	{		
		Patient returnPatient = patientBO.getByBsn(new BigInteger("0"));	
		Patient returnPatient2 = patientBO.getByBsn(null);
		
		assertNull(returnPatient);
		assertNull(returnPatient2);
	}
}
