package nl.pharmapartners.services.endpoints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import nl.pharmapartners.services.businessobjects.PatientBO;
import nl.pharmapartners.services.patientservice.v1.GenderType;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsRequest;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ObjectFactory;
import nl.pharmapartners.services.patientservice.v1.Patient;
import nl.pharmapartners.services.patientservice.v1.ResultCodeType;

@RunWith(SpringRunner.class)
public class PatientEndpointTest 
{
	@TestConfiguration
    static class PatientBOTestContextConfiguration 
    {
        @Bean
        public PatientEndpoint patientEndpoint()
        {
        	  return new PatientEndpoint();
        }
    }
	
	@Autowired
	private PatientEndpoint patientEndpoint;
	
	@MockBean
	private PatientBO patientBO;
	
	@Before
	public void setUp() throws DatatypeConfigurationException 
	{
		Patient patient = new Patient();
		patient.setBSN(new BigInteger("123456789"));
		patient.setFirstName("Bart1");
		patient.setName("Bollen1");
		patient.setBirthdate(DatatypeFactory.newInstance().newXMLGregorianCalendar((new GregorianCalendar(1988, Calendar.OCTOBER, 2))));
		patient.setGender(GenderType.M);
		
        when(patientBO.getByBsn(patient.getBSN())).thenReturn(patient);
    }
	
	@Test
	public void getPatientDetails_ReturnPatientResponse() throws DatatypeConfigurationException
	{
		ObjectFactory factory = new ObjectFactory();
		
		GetPatientDetailsRequest getPatientDetailsRequest = factory.createGetPatientDetailsRequest();
		getPatientDetailsRequest.setBSN(new BigInteger("123456789"));
		JAXBElement<GetPatientDetailsRequest> request = new JAXBElement<GetPatientDetailsRequest>(new QName("GetPatientDetailsRequest"), GetPatientDetailsRequest.class, getPatientDetailsRequest);
		
		GetPatientDetailsResponse getPatientDetailsResponse = factory.createGetPatientDetailsResponse();
		
		Patient patient = new Patient();
		patient.setBSN(new BigInteger("123456789"));
		patient.setFirstName("Bart1");
		patient.setName("Bollen1");
		patient.setBirthdate(DatatypeFactory.newInstance().newXMLGregorianCalendar((new GregorianCalendar(1988, Calendar.OCTOBER, 2))));
		patient.setGender(GenderType.M);
		
		getPatientDetailsResponse.setPatient(patient);
		getPatientDetailsResponse.setResultCode(ResultCodeType.OK);
		
		JAXBElement<GetPatientDetailsResponse> expectedResponse = new JAXBElement<GetPatientDetailsResponse>(new QName("GetPatientDetailsResponse"), GetPatientDetailsResponse.class, getPatientDetailsResponse);
		
		JAXBElement<GetPatientDetailsResponse> response = patientEndpoint.getPatientDetails(request);
		
		assertEquals(response.getValue().getPatient().getBSN(), expectedResponse.getValue().getPatient().getBSN());
		assertEquals(response.getValue().getPatient().getFirstName(), expectedResponse.getValue().getPatient().getFirstName());
		assertEquals(response.getValue().getPatient().getName(), expectedResponse.getValue().getPatient().getName());
		assertEquals(response.getValue().getPatient().getBirthdate().toGregorianCalendar().getTime(), expectedResponse.getValue().getPatient().getBirthdate().toGregorianCalendar().getTime());
		assertEquals(response.getValue().getPatient().getGender(), expectedResponse.getValue().getPatient().getGender());
	}
	
	@Test
	public void getPatientDetails_ReturnError() throws DatatypeConfigurationException
	{
		ObjectFactory factory = new ObjectFactory();
		
		GetPatientDetailsRequest getPatientDetailsRequest = factory.createGetPatientDetailsRequest();
		getPatientDetailsRequest.setBSN(new BigInteger("0"));
		JAXBElement<GetPatientDetailsRequest> request = new JAXBElement<GetPatientDetailsRequest>(new QName("GetPatientDetailsRequest"), GetPatientDetailsRequest.class, getPatientDetailsRequest);
		
		GetPatientDetailsResponse getPatientDetailsResponse = factory.createGetPatientDetailsResponse();
		getPatientDetailsResponse.setPatient(null);
		getPatientDetailsResponse.setResultCode(ResultCodeType.OK);
		
		JAXBElement<GetPatientDetailsResponse> response = patientEndpoint.getPatientDetails(request);
		
		assertNull(response.getValue().getPatient());
		assertEquals(response.getValue().getResultCode(), ResultCodeType.ERROR);
	}
}
