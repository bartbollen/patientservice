package nl.pharmapartners.services.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.pharmapartners.services.entities.Patient;
import nl.pharmapartners.services.patientservice.v1.GenderType;
import nl.pharmapartners.services.repositories.IPatientRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PatientRepositoryIntegrationTest 
{
	@Autowired
    private IPatientRepository patientRepository;
	
	@Test
	public void getByBsn_ReturnPatient() 
	{
		Patient patient = new Patient();
		patient.setBSN(new BigInteger("123456789"));
		patient.setFirstName("Bart");
		patient.setName("Bollen");
		patient.setBirthdate(new GregorianCalendar(1988, Calendar.OCTOBER, 2).getTime());
		patient.setGender(GenderType.M);
		
	    Patient found = patientRepository.getByBsn(patient.getBSN());
	 
	    assertEquals(found.getName(), patient.getName());
	    assertEquals(found.getFirstName(), patient.getFirstName());
	    assertEquals(found.getBirthdate().getTime(), patient.getBirthdate().getTime());
	    assertEquals(found.getGender(), patient.getGender());
	}
	
	@Test
	public void getByBsn_ReturnNull() 
	{	
	    Patient found = patientRepository.getByBsn(new BigInteger("0"));
	 
	    assertNull(found);
	}
}
